# ons

An eventually consistent name system working on the principle of user preference and authentic choice.

## User Preference

In the ON System user's domain name lookups result in a list of possible results. If only a single result is present, that will be used by the ONS. Otherwise the user chooses between all the available options - a choice that is rembered as user preference.

## Authentic Choice

In order for an authentic choice to be made, users get the results to a ONS Lookup sorted according to the number of peers each result has been choosen by.

Thus allowing for users to make a choice based on the choices of the peers around them and in their direct network environment.


By basing this system on OrbitDB and IPFS it is entirely decentralised and uncensorable without control by a central authority, like the IANA or ICAAN.
